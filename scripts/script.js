var countArrowClick = 1;
var countClickDetails = 1;
var i = 1;
var auto;

var images = new Array();
//Start
$(document).ready(function () {
    preloadImages(
        "https://www.beatsbydre.com/content/dam/beats/global/logo.svg",
        "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0002_rgb_MP162-RGB-bttm_V2.png",
        "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0053_rgb_MP582-RGB-bttm_V2.png",
        "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0056_rgb_MNET2-RGB-bttm.png",
        "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/solo3wireless-matrix-colorselector_0005_MUH42_Solo3WL-SatinGold_RGB-BTTM_v4.png",
        "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0052_rgb_MP582-RGB-front_V2.png",
        "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0051_rgb_MP582-RGB-side_V2.png",
        "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0055_rgb_MNET2-RGB-front.png",
        "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0054_rgb_MNET2-RGB-side.png",
        "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/solo3wireless-matrix-colorselector_0001_MUH42_Solo3WL-SatinGold_RGB-3QL_v4.png",
        "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/solo3wireless-matrix-colorselector_0003_MUH42_Solo3WL-SatinGold_RGB-SIDE_v3.png",
    );
    mouseEvents();
    animate();
    //Auto Animate Slider
    function AutoAnimate() {
        //counter 1
        if (countArrowClick === 1) {
            $(".arrow_right").unbind('click');
            $("#bg_content" + countArrowClick).animate({
                'left': +300
            }, 600);
            $("#bg_content" + (countArrowClick + 1)).animate({
                'left': 0
            }, {
                duration: 600,
                complete: function () {
                    $(".arrow_right").bind('click', _RightArrow);
                }
            });
            TweenMax.to('.details', 0.5, {
                top: 68
            });
            countClickDetails = 0;
            $(".legal").html("view details");
            i = 2;
            $("#slider").removeClass("bg_1").addClass("bg_2");
            $("#productName").html("Matt Black");
            $(".card").css("background", "#696666");
            $(".left img").attr("src", "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0052_rgb_MP582-RGB-front_V2.png");
            $(".right img").attr("src", "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0051_rgb_MP582-RGB-side_V2.png");
            $(".left").css("border-color", "#858181");
            $(".details").css("background", "#151414");
            $(".details p").css("border-color", "#c7c2c2");

        }
        //counter 2
        if (countArrowClick === 2) {
            $(".arrow_right").unbind('click');
            $("#bg_content" + countArrowClick).animate({
                'left': +300
            }, 600);
            $("#bg_content" + (countArrowClick + 1)).animate({
                'left': 0
            }, {
                duration: 600,
                complete: function () {
                    $(".arrow_right").bind('click', _RightArrow);
                }
            });
            TweenMax.to('.details', 0.5, {
                top: 68
            });
            countClickDetails = 0;
            $(".legal").html("view details");
            i = 3;
            $("#slider").removeClass("bg_2").addClass("bg_3");
            $("#productName").html("Rose Gold");
            $(".card").css("background", "#c5b5bc");
            $(".left img").attr("src", "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0055_rgb_MNET2-RGB-front.png");
            $(".right img").attr("src", "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0054_rgb_MNET2-RGB-side.png");
            $(".left").css("border-color", "#dad1d4");
            $(".details").css("background", "#a48b98");
            $(".details p").css("border-color", "#c6b8be");
        }
        //counter 3
        if (countArrowClick === 3) {
            $(".arrow_right").unbind('click');
            $("#bg_content" + countArrowClick).animate({
                'left': +300
            }, 600);
            $("#bg_content" + (countArrowClick + 1)).animate({
                'left': 0
            }, {
                duration: 600,
                complete: function () {
                    $(".arrow_right").bind('click', _RightArrow);
                }
            });

            TweenMax.to('.details', 0.5, {
                top: 68
            });
            countClickDetails = 0;
            $(".legal").html("view details");
            i = 4;
            $("#slider").removeClass("bg_3").addClass("bg_4");
            $("#productName").html("Satin Gold");
            $(".card").css("background", "#d0bbab");
            $(".left img").attr("src", "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/solo3wireless-matrix-colorselector_0001_MUH42_Solo3WL-SatinGold_RGB-3QL_v4.png");
            $(".right img").attr("src", "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/solo3wireless-matrix-colorselector_0003_MUH42_Solo3WL-SatinGold_RGB-SIDE_v3.png");
            $(".left").css("border-color", "#ebdfd7");
            $(".details").css("background", "#b59a84");
            $(".details p").css("border-color", "#e2d2c8");
        }
        //others
        countArrowClick++;
        if (countArrowClick > 3) {
            countArrowClick = 3;
        }
    }
    //Toggle right arrow animation
    function _RightArrow() {
        clearInterval(auto);
        //counter 1
        if (countArrowClick === 1) {
            $(".arrow_right").unbind('click');
            $("#bg_content" + countArrowClick).animate({
                'left': +300
            }, 600);
            $("#bg_content" + (countArrowClick + 1)).animate({
                'left': 0
            }, {
                duration: 600,
                complete: function () {
                    $(".arrow_right").bind('click', _RightArrow);
                }
            });
            TweenMax.to('.details', 0.5, {
                top: 68
            });
            countClickDetails = 0;
            $(".legal").html("view details");
            i = 2;
            $("#slider").removeClass("bg_1").addClass("bg_2");
            $("#productName").html("Matt Black");
            $(".card").css("background", "#696666");
            $(".left img").attr("src", "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0052_rgb_MP582-RGB-front_V2.png");
            $(".right img").attr("src", "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0051_rgb_MP582-RGB-side_V2.png");
            $(".left").css("border-color", "#858181");
            $(".details").css("background", "#151414");
            $(".details p").css("border-color", "#c7c2c2");

        }
        //counter2
        if (countArrowClick === 2) {
            $(".arrow_right").unbind('click');
            $("#bg_content" + countArrowClick).animate({
                'left': +300
            }, 600);
            $("#bg_content" + (countArrowClick + 1)).animate({
                'left': 0
            }, {
                duration: 600,
                complete: function () {
                    $(".arrow_right").bind('click', _RightArrow);
                }
            });
            TweenMax.to('.details', 0.5, {
                top: 68
            });
            countClickDetails = 0;
            $(".legal").html("view details");
            i = 3;
            $("#slider").removeClass("bg_2").addClass("bg_3");
            $("#productName").html("Rose Gold");
            $(".card").css("background", "#c5b5bc");
            $(".left img").attr("src", "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0055_rgb_MNET2-RGB-front.png");
            $(".right img").attr("src", "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0054_rgb_MNET2-RGB-side.png");
            $(".left").css("border-color", "#dad1d4");
            $(".details").css("background", "#a48b98");
            $(".details p").css("border-color", "#c6b8be");
        }
        //counter 3
        if (countArrowClick === 3) {
            $(".arrow_right").unbind('click');
            $("#bg_content" + countArrowClick).animate({
                'left': +300
            }, 600);
            $("#bg_content" + (countArrowClick + 1)).animate({
                'left': 0
            }, {
                duration: 600,
                complete: function () {
                    $(".arrow_right").bind('click', _RightArrow);
                }
            });

            TweenMax.to('.details', 0.5, {
                top: 68
            });
            countClickDetails = 0;
            $(".legal").html("view details");
            i = 4;
            $("#slider").removeClass("bg_3").addClass("bg_4");
            $("#productName").html("Satin Gold");
            $(".card").css("background", "#d0bbab");
            $(".left img").attr("src", "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/solo3wireless-matrix-colorselector_0001_MUH42_Solo3WL-SatinGold_RGB-3QL_v4.png");
            $(".right img").attr("src", "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/solo3wireless-matrix-colorselector_0003_MUH42_Solo3WL-SatinGold_RGB-SIDE_v3.png");
            $(".left").css("border-color", "#ebdfd7");
            $(".details").css("background", "#b59a84");
            $(".details p").css("border-color", "#e2d2c8");
        }
        //others
        countArrowClick++;
        if (countArrowClick > 3) {
            countArrowClick = 3;
        }
    }
    //Toggle left arrow animation
    function _LeftArrow() {
        clearInterval(auto);
        countArrowClick--;
        if (countArrowClick < 0) {
            countArrowClick = 0;
        }
        //counter 2
        if (countArrowClick === 2) {
            $(".arrow_left").unbind('click');
            $("#bg_content" + (countArrowClick + 1)).animate({
                'left': 0
            }, 600);
            $("#bg_content" + (countArrowClick + 2)).animate({
                'left': -300
            }, {
                duration: 600,
                complete: function () {
                    $(".arrow_left").bind('click', _LeftArrow);
                }
            });
            TweenMax.to('.details', 0.5, {
                top: 68
            });
            countClickDetails = 0;
            $(".legal").html("view details");
            i = 3;
            $("#slider").removeClass("bg_4").addClass("bg_3");
            $("#productName").html("Rose Gold");
            $(".card").css("background", "#c5b5bc");
            $(".left img").attr("src", "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0055_rgb_MNET2-RGB-front.png");
            $(".right img").attr("src", "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0054_rgb_MNET2-RGB-side.png");
            $(".left").css("border-color", "#dad1d4");
            $(".details").css("background", "#a48b98");
            $(".details p").css("border-color", "#c6b8be");
        }
        //counter 1
        if (countArrowClick === 1) {
            $(".arrow_left").unbind('click');
            $("#bg_content" + (countArrowClick + 1)).animate({
                'left': 0
            }, 600);
            $("#bg_content" + (countArrowClick + 2)).animate({
                'left': -300
            }, {
                duration: 600,
                complete: function () {
                    $(".arrow_left").bind('click', _LeftArrow);
                }
            });
            TweenMax.to('.details', 0.5, {
                top: 68
            });
            countClickDetails = 0;
            $(".legal").html("view details");
            i = 2;
            $("#slider").removeClass("bg_3").addClass("bg_2");
            $("#productName").html("Matt Black");
            $(".card").css("background", "#696666");
            $(".left img").attr("src", "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0052_rgb_MP582-RGB-front_V2.png");
            $(".right img").attr("src", "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0051_rgb_MP582-RGB-side_V2.png");
            $(".left").css("border-color", "#858181");
            $(".details").css("background", "#151414");
            $(".details p").css("border-color", "#c7c2c2");
        }
        //counter 0
        if (countArrowClick === 0) {
            $(".arrow_left").unbind('click');
            $("#bg_content" + (countArrowClick + 1)).animate({
                'left': 0
            }, 600);
            $("#bg_content" + (countArrowClick + 2)).animate({
                'left': -300
            }, {
                duration: 600,
                complete: function () {
                    $(".arrow_left").bind('click', _LeftArrow);
                }
            });
            TweenMax.to('.details', 0.5, {
                top: 68
            });
            countClickDetails = 0;
            $(".legal").html("view details");
            i = 1;
            $("#slider").removeClass("bg_2").addClass("bg_1");
            $("#productName").html("Red");
            $(".card").css("background", "#d04040");
            $(".left img").attr("src", "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0000_rgb_MP162-RGB-front_V2.png");
            $(".right img").attr("src", "https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0001_rgb_MP162-RGB-side_V2.png");
            $(".left").css("border-color", "#e85959");
            $(".details").css("background", "#9a140e");
            $(".details p").css("border-color", "#cd3e3f");
        }
    }
    //animation on frames
    function animate() {
        TweenMax.to('.txt', 1, {
            autoAlpha: 1
        });
        TweenMax.to('.txt', 1, {
            autoAlpha: 0,
            delay: 2.5
        });
        TweenMax.to('#header', 1, {
            autoAlpha: 1,
            delay: 3.5
        });
        TweenMax.to('#header', 1, {
            top: 0,
            delay: 4
        });
        TweenMax.to('.logo', 1, {
            width: 73,
            delay: 4,
            onComplete: function () {
                getData()
            }
        });
    }
    //get the call once animate is done
    function getData() {
        TweenMax.to('#intro', 1, {
            autoAlpha: 0
        });
        TweenMax.to('.txt2', 1, {
            autoAlpha: 1
        });

        TweenMax.to('.clickbtn', 1, {
            autoAlpha: 1,
            delay: 1,
            onComplete: function () {
                auto = setInterval(function () {
                    AutoAnimate();
                }, 3000)
            }
        });
    }
    //mouse events 
    function mouseEvents() {
        $(".arrow_right").bind('click', _RightArrow);
        $(".arrow_left").bind('click', _LeftArrow);
        //bottom left hover
        $(".left").mouseover(function () {
            if (i == 1) {
                $(".left").css("background", "#ba2929");
            } else if (i == 2) {
                $(".left").css("background", "#898484");
            } else if (i == 3) {
                $(".left").css("background", "#d6ced1");
            } else if (i == 4) {
                $(".left").css("background", "#e3d5ca");
            } else {
                $(".left").css("background", "#ba2929");
                $("#bg_content1 .productImg img").attr('src', 'https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0000_rgb_MP162-RGB-front_V2.png');
            }

            $("#bg_content" + i + " .productImg img").attr('src', $(".left img").prop('src'));
        });
        // left right mouse out
        $(".left, .right").mouseout(function () {
            $(".left, .right").css("background", "transparent");
            var defaultImgout;
            if (i == 1) {
                defaultImgout = 'https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0002_rgb_MP162-RGB-bttm_V2.png';
            } else if (i == 2) {
                defaultImgout = 'https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0053_rgb_MP582-RGB-bttm_V2.png';
            } else if (i == 3) {
                defaultImgout = 'https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0056_rgb_MNET2-RGB-bttm.png';
            } else if (i == 4) {
                defaultImgout = 'https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/solo3wireless-matrix-colorselector_0005_MUH42_Solo3WL-SatinGold_RGB-BTTM_v4.png';
            }else{
                $("#bg_content1 .productImg img").attr('src', 'https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0002_rgb_MP162-RGB-bttm_V2.png');
            }

            $("#bg_content" + i + " .productImg img").attr('src', defaultImgout);
        });
        //bottom right hover
        $(".right").mouseover(function () {
            if (i == 1) {
                $(".right").css("background", "#ba2929");
            } else if (i == 2) {
                $(".right").css("background", "#898484");
            } else if (i == 3) {
                $(".right").css("background", "#d6ced1");
            } else if (i == 4) {
                $(".right").css("background", "#e3d5ca");
            } else {
                $(".right").css("background", "#ba2929");
                $("#bg_content1 .productImg img").attr('src', 'https://www.beatsbydre.com/content/dam/beats/web/pdp/beats-solo3-wireless/color_selector/_0001_rgb_MP162-RGB-side_V2.png');
            }
            $("#bg_content" + i + " .productImg img").attr('src', $(".right img").prop('src'));
        });
        //legal click
        $(".legal").click(function () {
            clearInterval(auto);
            if (countClickDetails === 1) {
                TweenMax.to('.details', 1, {
                    top: 0
                });
                $(".legal").html("close");
            } else if (countClickDetails === 2) {
                TweenMax.to('.details', 1, {
                    top: 68
                });
                countClickDetails = 0;
                $(".legal").html("view details");
            }

            countClickDetails++;
        });
        //Click to URL
        $(".clickbtn").click(function () {
            window.open('https://www.beatsbydre.com/uk/', '_blank');
        });
    }
    //preloadImages
    function preloadImages() {
        for (i = 0; i < preloadImages.arguments.length; i++) {
            images[i] = new Image();
            images[i].src = preloadImages.arguments[i]
        }
    }
});